package com.cluster.map.clusterapp.ui.adapters;

import android.arch.lifecycle.Observer;
import android.arch.lifecycle.ViewModelProviders;
import android.databinding.DataBindingUtil;
import android.location.Address;
import android.support.annotation.Nullable;
import android.support.v4.app.FragmentActivity;
import android.util.Log;
import android.view.View;

import com.cluster.map.clusterapp.R;
import com.cluster.map.clusterapp.databinding.InfoWindowBinding;
import com.cluster.map.clusterapp.db.entity.PointEntity;
import com.cluster.map.clusterapp.viewmodel.PointViewModel;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.model.Marker;
import com.google.gson.Gson;

public class CustomInfoWindowAdapter implements GoogleMap.InfoWindowAdapter {
    private FragmentActivity context;

    final InfoWindowBinding mBinding;
    final PointViewModel model;
    private Marker mSelectedMarker;

    public CustomInfoWindowAdapter(FragmentActivity context){
        this.context = context;
        mBinding = DataBindingUtil.inflate(context.getLayoutInflater(), R.layout.info_window,
                null, false);
        model = ViewModelProviders.of(context).get(PointViewModel.class);
        mBinding.setLifecycleOwner(context);
        mBinding.setModel(model);
        subscribeToModel(model);
    }

    @Override
    public View getInfoWindow(Marker marker) {
        int id = (int) marker.getTag();
        mSelectedMarker = marker;
        model.setPointId(id);
        mBinding.executePendingBindings();
        return mBinding.getRoot();
    }

    @Override
    public View getInfoContents(Marker marker) {
        return null;
    }

    private void subscribeToModel(final PointViewModel model) {
        // Observe point data
        model.getObservablePoint().observe(context, new Observer<PointEntity>() {
            @Override
            public void onChanged(@Nullable PointEntity pointEntity) {
                if (pointEntity != null) {
                    model.setPoint(pointEntity);

                    updateInfoWindow();
                }
            }
        });

        // Observe point address
        model.getObservableAddress().observe(context, new Observer<Address>() {
            @Override
            public void onChanged(@Nullable Address address) {
                Log.d("Max", "address = "+new Gson().toJson(address));
                model.setPointAddress(address);

                updateInfoWindow();
            }
        });
    }

    /**
     * Updates current info window ui
     */
    public void updateInfoWindow() {
        mBinding.setMarker(mSelectedMarker);
        if (mSelectedMarker != null && mSelectedMarker.isInfoWindowShown()) {
            mSelectedMarker.showInfoWindow();
        }
    }
}