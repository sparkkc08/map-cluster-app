package com.cluster.map.clusterapp.ui.adapters;

import android.databinding.BindingAdapter;
import android.location.Address;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.cluster.map.clusterapp.R;
import com.google.android.gms.maps.model.Marker;
import com.squareup.picasso.Callback;
import com.squareup.picasso.Picasso;
import com.squareup.picasso.Transformation;

import java.util.ArrayList;
import java.util.List;

import jp.wasabeef.picasso.transformations.RoundedCornersTransformation;

public class BindingAdapters {
    /**
     * Set visibility for provided view
     * @param view
     * @param show
     */
    @BindingAdapter("visible")
    public static void showHide(View view, boolean show) {
        view.setVisibility(show ? View.VISIBLE : View.GONE);
    }

    /**
     * Populate provided view with full address string
     * @param view
     * @param address
     */
    @BindingAdapter("address")
    public static void setFullAddress(View view, Address address) {
        if(view instanceof TextView) {
            if(address != null && address.getMaxAddressLineIndex() >= 0) {
                List<String> addressLines = new ArrayList<>();
                for (int i = 0; i <= address.getMaxAddressLineIndex(); i++) {
                    addressLines.add(address.getAddressLine(i));
                }
                ((TextView) view).setText(TextUtils.join(", ", addressLines));
            } else {
                ((TextView) view).setText("");
            }
        }
    }

    /**
     * Download provided image url and set it into view
     * @param imageView
     * @param url
     */
    @BindingAdapter("imageUrl")
    public static void bindImage(ImageView imageView, String url) {
        Picasso.Builder builder = new Picasso.Builder(imageView.getContext());

        builder.build().
                load(url).
                noFade().
                placeholder(R.mipmap.ic_launcher).
                error(R.drawable.googleg_standard_color_18).
                into(imageView);
    }

    /**
     * Download provided image url, set picture into info window and update it
     * @param imageView
     * @param url
     * @param marker
     */
    @BindingAdapter({"markerImageUrl", "marker"})
    public static void bindImageWithMarker(ImageView imageView, String url, Marker marker) {
        //Just temporary trick to avoid reloading random image every time after address was updated
        if(marker != null) {
            if(TextUtils.equals(marker.getId(), (String) imageView.getTag())) {
                return;
            } else {
                imageView.setTag(marker.getId());
                Picasso.Builder builder = new Picasso.Builder(imageView.getContext());

                final Transformation transformation = new RoundedCornersTransformation(20, 0, RoundedCornersTransformation.CornerType.TOP_LEFT);
                builder.build().
                        load(url).
                        noFade().
                        placeholder(R.mipmap.ic_launcher).
                        error(R.drawable.googleg_standard_color_18).
                        transform(transformation).
                        into(imageView, new Callback() {
                            @Override
                            public void onSuccess() {
                                if(marker.isInfoWindowShown()) {
                                    marker.showInfoWindow();
                                }
                            }

                            @Override
                            public void onError() {
                                if(marker.isInfoWindowShown()) {
                                    marker.showInfoWindow();
                                }
                                imageView.setTag(null);
                            }
                        });
            }
        }
    }
}
