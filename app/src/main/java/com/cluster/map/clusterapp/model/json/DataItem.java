package com.cluster.map.clusterapp.model.json;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class DataItem {
    @SerializedName("items")
    private List<Item> items;
    @SerializedName("itemsCount")
    private int itemscount;
    @SerializedName("pagesCount")
    private int pagescount;

    public List<Item> getItems() {
        return items;
    }
}
