package com.cluster.map.clusterapp.viewmodel;

import android.app.Application;
import android.arch.lifecycle.AndroidViewModel;
import android.arch.lifecycle.LiveData;
import android.arch.lifecycle.ViewModel;
import android.arch.lifecycle.ViewModelProvider;
import android.databinding.ObservableField;
import android.support.annotation.NonNull;

import com.cluster.map.clusterapp.BasicApp;
import com.cluster.map.clusterapp.DataRepository;
import com.cluster.map.clusterapp.db.entity.PointEntity;


public class SinglePointViewModel extends AndroidViewModel {

    private final LiveData<PointEntity> mObservablePoint;
    private final DataRepository mRepository;

    public ObservableField<PointEntity> point = new ObservableField<>();

    private final int mPointId;


    public SinglePointViewModel(@NonNull Application application, DataRepository repository,
                                final int pointId) {
        super(application);
        mPointId = pointId;
        mRepository = repository;
        mObservablePoint = mRepository.loadPoint(mPointId);
    }

    public LiveData<PointEntity> getObservablePoint() {
        return mObservablePoint;
    }

    public void setPoint(PointEntity point) {
        this.point.set(point);
    }

    public void setPointRating(float rating) {
        mRepository.updatePointRating(mPointId, rating);
    }

    /**
     * A creator is used to inject the point ID into the ViewModel
     * <p>
     * This creator is to showcase how to inject dependencies into ViewModels. It's not
     * actually necessary in this case, as the point ID can be passed in a public method.
     */
    public static class Factory extends ViewModelProvider.NewInstanceFactory {

        @NonNull
        private final Application mApplication;

        private final int mPointId;

        private final DataRepository mRepository;

        public Factory(@NonNull Application application, int productId) {
            mApplication = application;
            mPointId = productId;
            mRepository = ((BasicApp) application).getRepository();
        }

        @Override
        public <T extends ViewModel> T create(Class<T> modelClass) {
            //noinspection unchecked
            return (T) new SinglePointViewModel(mApplication, mRepository, mPointId);
        }
    }
}
