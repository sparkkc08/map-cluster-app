package com.cluster.map.clusterapp.viewmodel.livedata;

import android.arch.lifecycle.LiveData;
import android.location.Address;

public class AddressLiveData extends LiveData<Address> {
    private AddressLiveData(Address address) {
        postValue(address);
    }
    public static LiveData<Address> create(Address address) {
        return new AddressLiveData(address);
    }
}
