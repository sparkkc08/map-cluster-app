package com.cluster.map.clusterapp.viewmodel;

import android.app.Application;
import android.arch.lifecycle.AndroidViewModel;
import android.arch.lifecycle.LiveData;
import android.arch.lifecycle.MutableLiveData;
import android.arch.lifecycle.Transformations;

import com.cluster.map.clusterapp.BasicApp;
import com.cluster.map.clusterapp.db.entity.PointEntity;
import com.cluster.map.clusterapp.model.PointItem;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

public class MapPointListViewModel extends AndroidViewModel {

    // MediatorLiveData can observe other LiveData objects and react on their emissions.
    private final LiveData<List<PointEntity>> mObservablePoints;
    final MutableLiveData<List<Integer>> mPointIds = new MutableLiveData<>();

    public MapPointListViewModel(Application application) {
        super(application);

        mObservablePoints = Transformations.switchMap(mPointIds, input -> {
            if (input == null) {
                return AbsentLiveData.create();
            }

            return ((BasicApp) application).getRepository().loadPoints(input);
        });
    }

    public void setPointItem(Collection<PointItem> markers) {
        if (markers.size() > 100 || (mPointIds.getValue() != null && (markers.containsAll(mPointIds.getValue()) && mPointIds.getValue().containsAll(markers)))) {
            return;
        }

        List<Integer> list = new ArrayList<>();
        for(PointItem pointItem : markers) {
            list.add(pointItem.getId());
        }

        mPointIds.setValue(list);
    }

    /**
     * Expose the LiveData Products query so the UI can observe it.
     */
    public LiveData<List<PointEntity>> getPoints() {
        return mObservablePoints;
    }

    public static class AbsentLiveData extends LiveData {
        private AbsentLiveData() {
            postValue(null);
        }
        public static   <T> LiveData<T> create() {
            //noinspection unchecked
            return new MapPointListViewModel.AbsentLiveData();
        }
    }
}
