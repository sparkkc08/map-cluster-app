package com.cluster.map.clusterapp.ui.utils;

import android.content.Context;

import com.cluster.map.clusterapp.model.PointItem;
import com.google.android.gms.maps.GoogleMap;
import com.google.maps.android.clustering.ClusterManager;
import com.google.maps.android.clustering.view.DefaultClusterRenderer;

public class CustomClusterRenderer extends DefaultClusterRenderer<PointItem> {
    public CustomClusterRenderer(Context context, GoogleMap map, ClusterManager<PointItem> clusterManager) {
        super(context, map, clusterManager);
    }
}
