package com.cluster.map.clusterapp.ui.fragments;

import android.arch.lifecycle.Observer;
import android.arch.lifecycle.ViewModelProviders;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RatingBar;

import com.cluster.map.clusterapp.R;
import com.cluster.map.clusterapp.databinding.PointFragmentBinding;
import com.cluster.map.clusterapp.db.entity.PointEntity;
import com.cluster.map.clusterapp.viewmodel.SinglePointViewModel;

public class PointFragment extends Fragment {

    private static final String KEY_POINT_ID = "point_id";
    private PointFragmentBinding mBinding;
    private SinglePointViewModel model;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        // Inflate this data binding layout
        mBinding = DataBindingUtil.inflate(inflater, R.layout.point_fragment, container, false);

        return mBinding.getRoot();
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        SinglePointViewModel.Factory factory = new SinglePointViewModel.Factory(
                getActivity().getApplication(), getArguments().getInt(KEY_POINT_ID));

        model = ViewModelProviders.of(this, factory)
                .get(SinglePointViewModel.class);

        mBinding.setProductViewModel(model);
        mBinding.setHandlers(this);
        subscribeToModel(model);
    }

    public void onRatingChanged(RatingBar ratingBar, float rating, boolean isFromUser) {
        if(isFromUser) {
            //Save new rating for the point
            model.setPointRating(rating);
        }
    }

    private void subscribeToModel(final SinglePointViewModel model) {
        // Observe point data
        model.getObservablePoint().observe(this, new Observer<PointEntity>() {
            @Override
            public void onChanged(@Nullable PointEntity pointEntity) {
                model.setPoint(pointEntity);
            }
        });
    }

    /** Creates point fragment for specific point ID */
    public static PointFragment forPoint(int pointId) {
        PointFragment fragment = new PointFragment();
        Bundle args = new Bundle();
        args.putInt(KEY_POINT_ID, pointId);
        fragment.setArguments(args);
        return fragment;
    }
}
