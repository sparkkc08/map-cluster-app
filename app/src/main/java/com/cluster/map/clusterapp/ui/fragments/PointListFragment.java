package com.cluster.map.clusterapp.ui.fragments;

import android.arch.lifecycle.Lifecycle;
import android.arch.lifecycle.Observer;
import android.arch.lifecycle.ViewModelProviders;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.cluster.map.clusterapp.R;
import com.cluster.map.clusterapp.databinding.ListFragmentBinding;
import com.cluster.map.clusterapp.db.entity.PointEntity;
import com.cluster.map.clusterapp.model.Point;
import com.cluster.map.clusterapp.ui.MainActivity;
import com.cluster.map.clusterapp.ui.adapters.PointAdapter;
import com.cluster.map.clusterapp.ui.callbacks.PointClickCallback;
import com.cluster.map.clusterapp.viewmodel.PointListViewModel;

import java.util.List;


public class PointListFragment extends Fragment {

    public static final String TAG = "PointListViewModel";

    private PointAdapter mPointAdapter;

    private ListFragmentBinding mBinding;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        mBinding = DataBindingUtil.inflate(inflater, R.layout.list_fragment, container, false);

        mPointAdapter = new PointAdapter(mPointClickCallback);
        mBinding.productsList.setAdapter(mPointAdapter);

        return mBinding.getRoot();
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        final PointListViewModel viewModel =
                ViewModelProviders.of(this).get(PointListViewModel.class);

        subscribeUi(viewModel);
    }

    private void subscribeUi(PointListViewModel viewModel) {
        // Update the list when the data changes
        viewModel.getPoints().observe(this, new Observer<List<PointEntity>>() {
            @Override
            public void onChanged(@Nullable List<PointEntity> myProducts) {
                if (myProducts != null) {
                    mBinding.setIsLoading(false);
                    mPointAdapter.setPointList(myProducts);
                } else {
                    mBinding.setIsLoading(true);
                }
                // espresso does not know how to wait for data binding's loop so we execute changes
                // sync.
                mBinding.executePendingBindings();
            }
        });
    }

    private final PointClickCallback mPointClickCallback = new PointClickCallback() {
        @Override
        public void onClick(Point point) {

            if (getLifecycle().getCurrentState().isAtLeast(Lifecycle.State.STARTED)) {
                ((MainActivity) getActivity()).show(point);
            }
        }
    };
}
