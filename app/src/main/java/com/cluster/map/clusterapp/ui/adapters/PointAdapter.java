package com.cluster.map.clusterapp.ui.adapters;

import android.databinding.DataBindingUtil;
import android.support.annotation.Nullable;
import android.support.v7.util.DiffUtil;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import com.cluster.map.clusterapp.R;
import com.cluster.map.clusterapp.databinding.ItemCardBinding;
import com.cluster.map.clusterapp.model.Point;
import com.cluster.map.clusterapp.ui.callbacks.PointClickCallback;

import java.util.List;
import java.util.Objects;

public class PointAdapter extends RecyclerView.Adapter<PointAdapter.PointViewHolder> {

    List<? extends Point> mPointList;

    @Nullable
    private final PointClickCallback mPointClickCallback;

    public PointAdapter(@Nullable PointClickCallback clickCallback) {
        mPointClickCallback = clickCallback;
    }

    public void setPointList(final List<? extends Point> pointList) {
        if (mPointList == null) {
            mPointList = pointList;
            notifyItemRangeInserted(0, pointList.size());
        } else {
            DiffUtil.DiffResult result = DiffUtil.calculateDiff(new DiffUtil.Callback() {
                @Override
                public int getOldListSize() {
                    return mPointList.size();
                }

                @Override
                public int getNewListSize() {
                    return pointList.size();
                }

                @Override
                public boolean areItemsTheSame(int oldItemPosition, int newItemPosition) {
                    return mPointList.get(oldItemPosition).getId() ==
                            pointList.get(newItemPosition).getId();
                }

                @Override
                public boolean areContentsTheSame(int oldItemPosition, int newItemPosition) {
                    Point newPoint = pointList.get(newItemPosition);
                    Point oldPoint = mPointList.get(oldItemPosition);
                    return newPoint.getId() == oldPoint.getId()
                            && Objects.equals(newPoint.getCurrency(), oldPoint.getCurrency())
                            && Objects.equals(newPoint.getName(), oldPoint.getName())
                            && newPoint.getPrice() == oldPoint.getPrice()
                            && newPoint.getLatitude() == oldPoint.getLatitude()
                            && newPoint.getLongitude() == oldPoint.getLongitude()
                            && newPoint.getDistance() == oldPoint.getDistance()
                            && newPoint.getRating() == oldPoint.getRating()
                            && newPoint.isFavorite() == oldPoint.isFavorite()
                            && TextUtils.equals(newPoint.getPicture(), oldPoint.getPicture());
                }
            });
            mPointList = pointList;
            result.dispatchUpdatesTo(this);
        }
    }

    @Override
    public PointViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        ItemCardBinding binding = DataBindingUtil
                .inflate(LayoutInflater.from(parent.getContext()), R.layout.item_card,
                        parent, false);
        binding.setCallback(mPointClickCallback);
        return new PointViewHolder(binding);
    }

    @Override
    public void onBindViewHolder(PointViewHolder holder, int position) {
        holder.binding.setPoint(mPointList.get(position));
        holder.binding.executePendingBindings();
    }

    @Override
    public int getItemCount() {
        return mPointList == null ? 0 : mPointList.size();
    }

    static class PointViewHolder extends RecyclerView.ViewHolder {

        final ItemCardBinding binding;

        public PointViewHolder(ItemCardBinding binding) {
            super(binding.getRoot());
            this.binding = binding;
        }
    }

    public void clear() {
        if(mPointList != null) {
            final int size = mPointList.size();
            mPointList.clear();
            notifyItemRangeRemoved(0, size);
        }
    }
}
