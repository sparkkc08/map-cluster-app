package com.cluster.map.clusterapp.ui.callbacks;


import com.cluster.map.clusterapp.model.Point;

public interface PointClickCallback {
    void onClick(Point point);
}
