package com.cluster.map.clusterapp.viewmodel;

import android.app.Application;
import android.arch.lifecycle.AndroidViewModel;
import android.arch.lifecycle.LiveData;
import android.arch.lifecycle.MediatorLiveData;

import com.cluster.map.clusterapp.BasicApp;
import com.cluster.map.clusterapp.db.entity.PointEntity;

import java.util.List;

public class PointListViewModel extends AndroidViewModel {

    // MediatorLiveData can observe other LiveData objects and react on their emissions.
    private final MediatorLiveData<List<PointEntity>> mObservablePoints;

    public PointListViewModel(Application application) {
        super(application);

        mObservablePoints = new MediatorLiveData<>();
        // set by default null, until we get data from the database.
        mObservablePoints.setValue(null);

        LiveData<List<PointEntity>> points = ((BasicApp) application).getRepository()
                .getPoints();

        // observe the changes of the products from the database and forward them
        mObservablePoints.addSource(points, mObservablePoints::setValue);
    }

    /**
     * Expose the LiveData Products query so the UI can observe it.
     */
    public LiveData<List<PointEntity>> getPoints() {
        return mObservablePoints;
    }
}
