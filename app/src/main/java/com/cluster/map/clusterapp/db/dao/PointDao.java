package com.cluster.map.clusterapp.db.dao;


import android.arch.lifecycle.LiveData;
import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.OnConflictStrategy;
import android.arch.persistence.room.Query;
import android.arch.persistence.room.Update;

import com.cluster.map.clusterapp.db.entity.PointEntity;

import java.util.List;

import static android.arch.persistence.room.OnConflictStrategy.REPLACE;

@Dao
public interface PointDao {
    @Query("SELECT * FROM points")
    LiveData<List<PointEntity>> loadAllPoints();

    @Insert(onConflict = REPLACE)
    void insertAll(List<PointEntity> points);

    @Query("select * from points where id = :pointId")
    LiveData<PointEntity> loadPoint(int pointId);

    @Query("select * from points where id = :pointId")
    PointEntity loadPointSync(int pointId);

    @Query("SELECT * FROM points WHERE id in (:pointIds)")
    LiveData<List<PointEntity>> loadByIds(List<Integer> pointIds);

    @Query("UPDATE points SET rating = :rating  WHERE id = :id")
    int updatePointRating(int id, float rating);
}
