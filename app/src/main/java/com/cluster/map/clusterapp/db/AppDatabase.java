package com.cluster.map.clusterapp.db;

import android.arch.lifecycle.LiveData;
import android.arch.lifecycle.MutableLiveData;
import android.arch.persistence.db.SupportSQLiteDatabase;
import android.arch.persistence.room.Database;
import android.arch.persistence.room.Room;
import android.arch.persistence.room.RoomDatabase;
import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.VisibleForTesting;
import android.util.Log;

import com.cluster.map.clusterapp.AppExecutors;
import com.cluster.map.clusterapp.db.dao.PointDao;
import com.cluster.map.clusterapp.db.entity.PointEntity;

import java.io.IOException;
import java.util.List;

@Database(entities = {PointEntity.class}, version = 1)
public abstract class AppDatabase extends RoomDatabase {

    private static AppDatabase sInstance;

    @VisibleForTesting
    public static final String DATABASE_NAME = "basic-sample-db";

    public abstract PointDao pointDao();

    private final MutableLiveData<Boolean> mIsDatabaseCreated = new MutableLiveData<>();

    public static AppDatabase getInstance(final Context context, final AppExecutors executors) {
        if (sInstance == null) {
            synchronized (AppDatabase.class) {
                if (sInstance == null) {
                    sInstance = buildDatabase(context.getApplicationContext(), executors);
                    sInstance.updateDatabaseCreated(context.getApplicationContext(), executors);
                }
            }
        }

        return sInstance;
    }

    /**
     * Build the database. {@link Builder#build()} sets up the database configuration,
     * creates a new instance of the database and populate it with data from json.
     * The SQLite database is only created when it's accessed for the first time.
     */
    private static AppDatabase buildDatabase(final Context appContext, final AppExecutors executors) {
        return Room.databaseBuilder(appContext, AppDatabase.class, DATABASE_NAME)
                .addCallback(new Callback() {
                    @Override
                    public void onCreate(@NonNull SupportSQLiteDatabase db) {
                        super.onCreate(db);
                        sInstance.createOrUpdateDatabase(appContext, executors);
                    }
                }).build();
    }

    /**
     * Check whether the database already exists and expose it via {@link #getDatabaseCreated()}
     */
    private void updateDatabaseCreated(final Context context, final AppExecutors executors) {
        if (context.getDatabasePath(DATABASE_NAME).exists()) {
            createOrUpdateDatabase(context, executors);
        }
    }

    private void createOrUpdateDatabase(final Context context, final AppExecutors executors) {
        executors.diskIO().execute(() -> {
            // Add a delay to simulate a long-running operation
            //addDelay();
            // Generate the data for pre-population
            AppDatabase database = AppDatabase.getInstance(context, executors);
            List<PointEntity> points = null;
            try {
                points = DataGenerator.readData(context.getAssets().open("map.json.txt"));
                insertData(database, points);
            } catch (IOException e) {
                Log.e(AppDatabase.class.getSimpleName(), e.toString());
            }

            // notify that the database was created and it's ready to be used
            database.setDatabaseCreated();
        });
    }

    private void setDatabaseCreated(){
        mIsDatabaseCreated.postValue(true);
    }

    private static void insertData(final AppDatabase database, final List<PointEntity> products) {
        database.runInTransaction(() -> {
            database.pointDao().insertAll(products);
        });
    }

    private static void addDelay() {
        try {
            Thread.sleep(4000);
        } catch (InterruptedException ignored) {
        }
    }

    public LiveData<Boolean> getDatabaseCreated() {
        return mIsDatabaseCreated;
    }
}
