package com.cluster.map.clusterapp.model;

import com.cluster.map.clusterapp.db.entity.PointEntity;
import com.google.android.gms.maps.model.LatLng;
import com.google.maps.android.clustering.ClusterItem;

import java.util.Objects;

public class PointItem implements ClusterItem {
    private final LatLng mPosition;
    private PointEntity pointEntity;
    private final int id;

    public PointItem(PointEntity pointEntity) {
        this.pointEntity = pointEntity;
        id = pointEntity.getId();
        mPosition = new LatLng(pointEntity.getLatitude(), pointEntity.getLongitude());
    }

    @Override
    public LatLng getPosition() {
        return mPosition;
    }

    @Override
    public String getTitle() {
        return pointEntity.getName();
    }

    @Override
    public String getSnippet() {
        return pointEntity.getCurrency();
    }

    public int getId() {
        return id;
    }

    @Override
    public boolean equals(Object obj) {
        if(obj instanceof PointItem) {
            return ((PointItem) obj).getId() == getId();
        }
        return false;
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }
}
