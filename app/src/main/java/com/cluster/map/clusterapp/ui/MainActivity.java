package com.cluster.map.clusterapp.ui;

import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import com.cluster.map.clusterapp.R;
import com.cluster.map.clusterapp.model.Point;
import com.cluster.map.clusterapp.ui.fragments.MapFragment;
import com.cluster.map.clusterapp.ui.fragments.PointFragment;
import com.cluster.map.clusterapp.ui.fragments.PointListFragment;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main_activity);

        // Add map fragment if this is first creation
        if (savedInstanceState == null) {
            MapFragment fragment = new MapFragment();

            getSupportFragmentManager().beginTransaction()
                    .add(R.id.fragment_container, fragment, PointListFragment.TAG).commit();
        }
    }

    /** Shows the point detail fragment*/
    public void show(Point point) {

        PointFragment pointFragment = PointFragment.forPoint(point.getId());

        getSupportFragmentManager()
                .beginTransaction()
                .addToBackStack("point")
                .add(R.id.fragment_container,
                        pointFragment, null).commit();
    }

    public void show(int point) {
        PointFragment pointFragment = PointFragment.forPoint(point);

        getSupportFragmentManager()
                .beginTransaction()
                .addToBackStack("point")
                .add(R.id.fragment_container,
                        pointFragment, null).commit();
    }
}
