package com.cluster.map.clusterapp.model;

public interface Point {
    int getId();
    String getName();
    String getCurrency();
    int getPrice();
    double getDistance();
    double getLongitude();
    double getLatitude();
    String getPicture();
    boolean isFavorite();
    float getRating();
}
