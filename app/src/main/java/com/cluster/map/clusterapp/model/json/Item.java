package com.cluster.map.clusterapp.model.json;

import com.google.gson.annotations.SerializedName;

public class Item {
    @SerializedName("distance")
    private double distance;
    @SerializedName("currency")
    private String currency;
    @SerializedName("price")
    private int price;
    @SerializedName("location")
    private Location location;
    @SerializedName("name")
    private String name;
    @SerializedName("id")
    private int id;

    public double getDistance() {
        return distance;
    }

    public String getCurrency() {
        return currency;
    }

    public int getPrice() {
        return price;
    }

    public Location getLocation() {
        return location;
    }

    public String getName() {
        return name;
    }

    public int getId() {
        return id;
    }
}
