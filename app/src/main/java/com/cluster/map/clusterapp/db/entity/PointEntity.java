package com.cluster.map.clusterapp.db.entity;


import android.arch.persistence.room.ColumnInfo;
import android.arch.persistence.room.Entity;
import android.arch.persistence.room.PrimaryKey;

import com.cluster.map.clusterapp.model.Point;

@Entity(tableName = "points")
public class PointEntity implements Point {
    @PrimaryKey
    private int id;

    private double longitude;
    private double latitude;
    private double distance;
    private String currency;
    private int price;
    private String name;
    private String picture;
    private boolean isFavorite;
    @ColumnInfo(name = "rating")
    private float rating;

    @Override
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    @Override
    public double getLongitude() {
        return longitude;
    }

    public void setLongitude(double longitude) {
        this.longitude = longitude;
    }

    @Override
    public double getLatitude() {
        return latitude;
    }

    public void setLatitude(double latitude) {
        this.latitude = latitude;
    }

    @Override
    public float getRating() {
        return rating;
    }

    public void setRating(float rating) {
        this.rating = rating;
    }

    @Override
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public String getCurrency() {
        return currency;
    }

    public void setCurrency(String currency) {
        this.currency = currency;
    }

    @Override
    public int getPrice() {
        return price;
    }

    @Override
    public double getDistance() {
        return distance;
    }

    public void setDistance(double distance) {
        this.distance = distance;
    }

    public void setPrice(int price) {
        this.price = price;
    }

    @Override
    public String getPicture() {
        return picture;
    }

    public void setPicture(String picture) {
        this.picture = picture;
    }

    @Override
    public boolean isFavorite() {
        return isFavorite;
    }

    public void setFavorite(boolean favorite) {
        isFavorite = favorite;
    }

    public PointEntity() {
    }

    public PointEntity(int id, double longitude, double latitude, String name, String currency,
                       int price, double distance, String picture, boolean isFavorite, float rating) {
        this.id = id;
        this.longitude = longitude;
        this.latitude = latitude;
        this.name = name;
        this.currency = currency;
        this.price = price;
        this.distance = distance;
        this.picture = picture;
        this.isFavorite = isFavorite;
        this.rating = rating;
    }

    public PointEntity(Point point) {
        this.id = point.getId();
        this.longitude = point.getLongitude();
        this.latitude = point.getLatitude();
        this.name = point.getName();
        this.currency = point.getCurrency();
        this.price = point.getPrice();
        this.distance = point.getDistance();
        this.picture = point.getPicture();
        this.isFavorite = point.isFavorite();
        this.rating = point.getRating();
    }
}
