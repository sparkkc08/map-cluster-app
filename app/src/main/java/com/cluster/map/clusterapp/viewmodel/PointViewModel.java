package com.cluster.map.clusterapp.viewmodel;

import android.app.Application;
import android.arch.lifecycle.AndroidViewModel;
import android.arch.lifecycle.LiveData;
import android.arch.lifecycle.MutableLiveData;
import android.arch.lifecycle.Transformations;
import android.databinding.ObservableField;
import android.location.Address;
import android.location.Geocoder;
import android.support.annotation.NonNull;

import com.cluster.map.clusterapp.BasicApp;
import com.cluster.map.clusterapp.DataRepository;
import com.cluster.map.clusterapp.db.entity.PointEntity;
import com.cluster.map.clusterapp.viewmodel.livedata.AbsentLiveData;
import com.cluster.map.clusterapp.viewmodel.livedata.AddressLiveData;
import com.google.android.gms.maps.model.LatLng;

import java.io.IOException;
import java.util.List;
import java.util.Locale;


public class PointViewModel extends AndroidViewModel {

    private final DataRepository mRepository;
    private LiveData<PointEntity> mObservablePoint;
    private LiveData<Address> mObservableAddress;
    final MutableLiveData<Integer> pointId = new MutableLiveData<>();
    final MutableLiveData<LatLng> pointLocation = new MutableLiveData<>();
    public ObservableField<PointEntity> point = new ObservableField<>();
    public ObservableField<Address> pointAddress = new ObservableField<>();

    private Geocoder mGeocoder;

    public PointViewModel(@NonNull Application application) {
        super(application);
        mRepository = ((BasicApp) application).getRepository();
        mGeocoder = new Geocoder(application, Locale.getDefault());

        mObservablePoint = Transformations.switchMap(pointId, input -> {
            if (input == null) {
                return AbsentLiveData.create();
            }

            return mRepository.loadPoint(input);
        });

        mObservableAddress = Transformations.switchMap(pointLocation, input -> {
            if (input == null) {
                return AbsentLiveData.create();
            }

            List<Address> addresses = null;

            try {
                addresses = mGeocoder.getFromLocation(input.latitude, input.longitude, 1);
            } catch (IOException e) {
                e.printStackTrace();
            }

            return (addresses == null || addresses.isEmpty()) ? null : AddressLiveData.create(addresses.get(0));
        });
    }

    public void setPointId(int id) {
        if (pointId.getValue() != null && pointId.getValue() == id) {
            return;
        }
        setPointAddress(null);
        pointId.setValue(id);
    }

    public void setPointLocation(double latitude, double longitude) {
        if (pointLocation.getValue() != null && Double.compare(pointLocation.getValue().latitude, latitude) == 0 &&
                Double.compare(pointLocation.getValue().longitude, longitude) == 0) {
            return;
        }
        pointLocation.setValue(new LatLng(latitude, longitude));
    }

    public void setPoint(PointEntity pointEntity) {
        point.set(pointEntity);
        setPointLocation(pointEntity.getLatitude(), pointEntity.getLongitude());
    }

    public LiveData<PointEntity> getObservablePoint() {
        return mObservablePoint;
    }

    public void setPointAddress(Address address) {
        pointAddress.set(address);
    }

    public LiveData<Address> getObservableAddress() {
        return mObservableAddress;
    }
}
