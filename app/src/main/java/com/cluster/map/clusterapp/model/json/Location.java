package com.cluster.map.clusterapp.model.json;

import com.google.gson.annotations.SerializedName;

public class Location {
    @SerializedName("longitude")
    private double longitude;
    @SerializedName("latitude")
    private double latitude;

    public double getLongitude() {
        return longitude;
    }

    public double getLatitude() {
        return latitude;
    }
}
