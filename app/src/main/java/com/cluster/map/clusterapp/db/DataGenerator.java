package com.cluster.map.clusterapp.db;

import android.text.TextUtils;
import android.util.Log;

import com.cluster.map.clusterapp.db.entity.PointEntity;
import com.cluster.map.clusterapp.model.json.DataItem;
import com.cluster.map.clusterapp.model.json.Item;
import com.google.gson.Gson;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;

/**
 * Generates data to pre-populate the database
 */
public class DataGenerator {
    private static final String RANDOM_PICTURE = "https://picsum.photos/200/?random";

    private static List<PointEntity> writePointsToDb(List<Item> items) {
        List<PointEntity> points = null;
        if(items != null && !items.isEmpty()) {
            points = new ArrayList<>(items.size());
            Random rnd = new Random();
            for (int i = 0; i < items.size(); i++) {
                Item item = items.get(i);

                PointEntity pointEntity = new PointEntity();

                if(item.getLocation() != null) {
                    pointEntity.setLatitude(item.getLocation().getLatitude());
                    pointEntity.setLongitude(item.getLocation().getLongitude());
                }

                if(TextUtils.isEmpty(item.getName())) {
                    pointEntity.setName("Point " + item.getId());
                } else {
                    pointEntity.setName(item.getName());
                }

                if(TextUtils.isEmpty(item.getCurrency())) {
                    pointEntity.setCurrency("USD");
                } else {
                    pointEntity.setCurrency(item.getCurrency());
                }

                if(item.getPrice() > 0) {
                    pointEntity.setPrice(item.getPrice());
                } else {
                    pointEntity.setPrice(rnd.nextInt(300));
                }

                if(item.getDistance() > 0) {
                    pointEntity.setDistance(item.getDistance());
                } else {
                    pointEntity.setDistance(rnd.nextInt(20) + rnd.nextFloat());
                }

                pointEntity.setId(item.getId());
                pointEntity.setPicture(RANDOM_PICTURE);
                pointEntity.setRating(item.getId() % 5 + rnd.nextFloat());
                points.add(pointEntity);
            }
        }

        return points;
    }

    public static List<PointEntity> readData(InputStream inputStream) {
        try {
            byte[] bytes = new byte[inputStream.available()];
            inputStream.read(bytes, 0, bytes.length);
            String json = new String(bytes);
            DataItem item = new Gson().fromJson(json, DataItem.class);
            if(item != null) {
                return writePointsToDb(item.getItems());
            }
        } catch (IOException e) {
            Log.e(DataGenerator.class.getSimpleName(), e.toString());
        }

        return null;
    }
}
