package com.cluster.map.clusterapp;


import android.arch.lifecycle.LiveData;
import android.arch.lifecycle.MediatorLiveData;
import android.arch.lifecycle.MutableLiveData;
import android.support.annotation.NonNull;
import android.util.Log;

import com.cluster.map.clusterapp.db.AppDatabase;
import com.cluster.map.clusterapp.db.entity.PointEntity;

import java.util.List;

/**
 * Repository handling the work with points.
 */

public class DataRepository {

    private static DataRepository sInstance;

    private final AppDatabase mDatabase;
    private MediatorLiveData<List<PointEntity>> mObservablePoints;
    private MutableLiveData<PointEntity> mObservablePoint;
    private final AppExecutors mAppExecutors;

    private DataRepository(final AppDatabase database, final AppExecutors appExecutors) {
        mDatabase = database;
        mAppExecutors = appExecutors;
        mObservablePoints = new MediatorLiveData<>();

        mObservablePoints.addSource(mDatabase.pointDao().loadAllPoints(),
                pointEntities -> {
                    if (mDatabase.getDatabaseCreated().getValue() != null) {
                        mObservablePoints.postValue(pointEntities);
                    }
                });
    }

    public static DataRepository getInstance(final AppDatabase database, AppExecutors appExecutors) {
        if (sInstance == null) {
            synchronized (DataRepository.class) {
                if (sInstance == null) {
                    sInstance = new DataRepository(database, appExecutors);
                }
            }
        }
        return sInstance;
    }

    /**
     * Get the list of products from the database and get notified when the data changes.
     */
    public LiveData<List<PointEntity>> getPoints() {
        return mObservablePoints;
    }

    /**
     * Get the list of products from the database and get notified when the data changes.
     */
    public LiveData<List<PointEntity>> getPoint() {
        return mObservablePoints;
    }

    public LiveData<PointEntity> getPointFromId(MutableLiveData point, final int pointId) {
        LiveData<PointEntity> pointEntity = loadPoint(pointId);
        point.setValue(pointEntity);
        return pointEntity;
    }

    public LiveData<PointEntity> loadPoint(final int pointId) {
        return mDatabase.pointDao().loadPoint(pointId);
    }

    public void updatePointRating(final int pointId, final float rating) {
        mAppExecutors.diskIO().execute(new Runnable() {
            @Override
            public void run() {
                mDatabase.pointDao().updatePointRating(pointId, rating);
            }
        });
    }

    public LiveData<List<PointEntity>> loadPoints(final List<Integer> ids) {
        return mDatabase.pointDao().loadByIds(ids);
    }
}
