package com.cluster.map.clusterapp.ui.fragments;

import android.arch.lifecycle.Lifecycle;
import android.arch.lifecycle.Observer;
import android.arch.lifecycle.ViewModelProviders;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.LinearSnapHelper;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;

import com.cluster.map.clusterapp.R;
import com.cluster.map.clusterapp.databinding.MapFragmentBinding;
import com.cluster.map.clusterapp.db.entity.PointEntity;
import com.cluster.map.clusterapp.model.Point;
import com.cluster.map.clusterapp.model.PointItem;
import com.cluster.map.clusterapp.ui.MainActivity;
import com.cluster.map.clusterapp.ui.adapters.CustomInfoWindowAdapter;
import com.cluster.map.clusterapp.ui.adapters.PointAdapter;
import com.cluster.map.clusterapp.ui.callbacks.PointClickCallback;
import com.cluster.map.clusterapp.ui.utils.CustomClusterRenderer;
import com.cluster.map.clusterapp.ui.utils.VerticalOffsetDecoration;
import com.cluster.map.clusterapp.viewmodel.MapPointListViewModel;
import com.cluster.map.clusterapp.viewmodel.PointListViewModel;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.google.android.gms.maps.model.Marker;
import com.google.maps.android.clustering.Cluster;
import com.google.maps.android.clustering.ClusterItem;
import com.google.maps.android.clustering.ClusterManager;

import java.util.Collection;
import java.util.List;

public class MapFragment extends Fragment implements OnMapReadyCallback, ClusterManager.OnClusterItemClickListener<PointItem>, ClusterManager.OnClusterClickListener<PointItem>, ClusterManager.OnClusterItemInfoWindowClickListener<PointItem>, GoogleMap.OnMapClickListener {

    private GoogleMap mMap;
    private MapFragmentBinding mBinding;
    private CustomClusterRenderer mClusterRenderer;
    private ClusterManager<PointItem> mClusterManager;
    private PointAdapter mPointAdapter;
    private MapPointListViewModel mapPointListViewModel;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        mBinding = DataBindingUtil.inflate(inflater, R.layout.map_fragment, container, false);

        SupportMapFragment mapFragment = (SupportMapFragment) getChildFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);

        mPointAdapter = new PointAdapter(mPointClickCallback);

        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getContext(), LinearLayoutManager.HORIZONTAL, false);
        mBinding.recyclerView.setLayoutManager(linearLayoutManager);

        mBinding.recyclerView.setAdapter(mPointAdapter);
        LinearSnapHelper snapHelper = new LinearSnapHelper() {
            @Override
            public int findTargetSnapPosition(RecyclerView.LayoutManager layoutManager, int velocityX, int velocityY) {
                int snapPosition = super.findTargetSnapPosition(layoutManager, velocityX, velocityY);
                int position = linearLayoutManager.findFirstVisibleItemPosition();
                //Check if user tries swipe first item
                if(snapPosition <= 0 && position == 0) {
                    animationHide();
                }

                return snapPosition;
            }
        };
        snapHelper.attachToRecyclerView(mBinding.recyclerView);
        mBinding.recyclerView.setOnFlingListener(snapHelper);
        mBinding.recyclerView.addItemDecoration(new VerticalOffsetDecoration(getActivity()));

        return mBinding.getRoot();
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        mapPointListViewModel = ViewModelProviders.of(this).get(MapPointListViewModel.class);
        subscribeClusterUi(mapPointListViewModel);
    }

    private void subscribeUi(PointListViewModel viewModel) {
        // Update the list when the data changes
        viewModel.getPoints().observe(this, new Observer<List<PointEntity>>() {
            @Override
            public void onChanged(@Nullable List<PointEntity> myPoints) {
                if (myPoints != null) {
                    mClusterManager.clearItems();

                    for(PointEntity pointEntity : myPoints) {
                        mClusterManager.addItem(new PointItem(pointEntity));
                    }

                    mClusterManager.cluster();
                }
                mBinding.executePendingBindings();
            }
        });
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;

        CustomInfoWindowAdapter adapter = new CustomInfoWindowAdapter(getActivity());

        mMap.getUiSettings().setZoomControlsEnabled(true);

        mClusterManager = new ClusterManager<>(getActivity(), mMap);
        mClusterRenderer = new CustomClusterRenderer(getActivity(), mMap, mClusterManager);
        mClusterManager.getMarkerCollection().setOnInfoWindowAdapter(adapter);
        mClusterManager.setRenderer(mClusterRenderer);
        mClusterManager.setOnClusterItemClickListener(this);
        mClusterManager.setOnClusterClickListener(this);
        mClusterManager.setOnClusterItemInfoWindowClickListener(this);

        mMap.setOnCameraIdleListener(mClusterManager);
        mMap.setOnMarkerClickListener(mClusterManager);
        mMap.setOnInfoWindowClickListener(mClusterManager);
        mMap.setInfoWindowAdapter(mClusterManager.getMarkerManager());
        mMap.setOnMapClickListener(this);

        final PointListViewModel viewModel =
                ViewModelProviders.of(this).get(PointListViewModel.class);

        subscribeUi(viewModel);
    }

    @Override
    public boolean onClusterItemClick(PointItem pointItem) {
        Marker marker = mClusterRenderer.getMarker(pointItem);
        marker.setTag(pointItem.getId());
        mMap.animateCamera(CameraUpdateFactory.newLatLng(marker.getPosition()),
                getResources().getInteger(android.R.integer.config_longAnimTime),
                new GoogleMap.CancelableCallback() {
                    @Override
                    public void onFinish() {
                        marker.showInfoWindow();
                    }

                    @Override
                    public void onCancel() {

                    }
                });

        return true;
    }

    @Override
    public boolean onClusterClick(Cluster<PointItem> cluster) {
        LatLngBounds.Builder builder = LatLngBounds.builder();
        Collection<PointItem> markers = cluster.getItems();

        //Check if zoom is max level then show bottom list
        if(mMap.getCameraPosition().zoom == mMap.getMaxZoomLevel()) {
            mapPointListViewModel.setPointItem(markers);
        } else {    //Other way increase zoom with all visible markers
            for (ClusterItem item : markers) {
                LatLng position = item.getPosition();
                builder.include(position);
            }

            final LatLngBounds bounds = builder.build();

            try {
                mMap.animateCamera(CameraUpdateFactory.newLatLngBounds(bounds, 100));
            } catch (Exception error) {
                Log.e(MapFragment.class.getSimpleName(), error.getMessage());
            }
        }

        return true;
    }

    private void subscribeClusterUi(MapPointListViewModel viewModel) {
        // Update the list when the data changes
        viewModel.getPoints().observe(this, new Observer<List<PointEntity>>() {
            @Override
            public void onChanged(@Nullable List<PointEntity> myPoints) {
                if (myPoints != null && !myPoints.isEmpty()) {
                    mPointAdapter.setPointList(myPoints);
                    Animation animation = AnimationUtils.loadAnimation(getContext(), R.anim.slide_in_right);
                    animation.setAnimationListener(new Animation.AnimationListener() {
                        @Override
                        public void onAnimationStart(Animation animation) {
                            mBinding.recyclerView.setVisibility(View.VISIBLE);
                            mMap.getUiSettings().setZoomControlsEnabled(false);
                        }

                        @Override
                        public void onAnimationEnd(Animation animation) {
                        }

                        @Override
                        public void onAnimationRepeat(Animation animation) {

                        }
                    });

                    mBinding.recyclerView.startAnimation(animation);
                }
                mBinding.executePendingBindings();
            }
        });
    }

    private final PointClickCallback mPointClickCallback = new PointClickCallback() {
        @Override
        public void onClick(Point point) {
            if (getLifecycle().getCurrentState().isAtLeast(Lifecycle.State.STARTED)) {
                ((MainActivity) getActivity()).show(point);
            }
        }
    };

    @Override
    public void onClusterItemInfoWindowClick(PointItem pointItem) {
        if (getLifecycle().getCurrentState().isAtLeast(Lifecycle.State.STARTED)) {
            ((MainActivity) getActivity()).show(pointItem.getId());
        }
    }

    @Override
    public void onMapClick(LatLng latLng) {
        animationHide();
    }

    /**
     * Hide bottom list if it visible
     */
    private void animationHide() {
        if(mBinding.recyclerView.getVisibility() == View.VISIBLE) {
            Animation animation = AnimationUtils.loadAnimation(getContext(), R.anim.slide_out_right);
            animation.setAnimationListener(new Animation.AnimationListener() {
                @Override
                public void onAnimationStart(Animation animation) {

                }

                @Override
                public void onAnimationEnd(Animation animation) {
                    mBinding.recyclerView.setVisibility(View.VISIBLE);
                    mMap.getUiSettings().setZoomControlsEnabled(true);
                    mPointAdapter.clear();
                }

                @Override
                public void onAnimationRepeat(Animation animation) {

                }
            });

            mBinding.recyclerView.startAnimation(animation);
        }
    }
}